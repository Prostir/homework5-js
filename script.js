'use strict';

function createNewUser() {
    let newUser = {
        userName: prompt("What is your name?"),
        userLastName: prompt("What is your lastName?"),
        birthday: prompt("Enter your birthday(dd.mm.yyyy)"),

        getAge: function () {
            let now = new Date();
            let currentYear = now.getFullYear();

            let date = +this.birthday.substring(0, 2);
            let month = +this.birthday.substring(3, 5);
            let year = +this.birthday.substring(6, 10);

            let birthDate = new Date(year, month, date);
            let birthYear = birthDate.getFullYear();
            let age = currentYear - birthYear;
            return console.log(age);
        },
        getPassword: function () {
            return console.log(this.userName.charAt(0).toUpperCase() + this.userLastName.toLowerCase() + this.birthday.substring(6, 10));
        }
    };
    newUser.getAge();
    newUser.getPassword();
}

createNewUser()
//
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
//
//     При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//     Создать метод getAge() который будет возвращать сколько пользователю лет.
//     Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
//



